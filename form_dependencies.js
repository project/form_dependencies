
/**
 * @file
 * Javascript helper functions for collapsing/expanding fields that have requirements with another
 * field.
 */

/**
 * Function for initiating the global dependency things.
 */
Drupal.formDependenciesInit = function() {
  // Loop through the dependency fields to disable them by default and append an alternative
  // required mark to the label element. For now, the HTML for the required mark is provided by the
  // module in Drupal.settings.required and appended to the 'label' element which is then hidden;
  // not very flexible and not theme-proof though.
  if (Drupal.settings && Drupal.settings.required) {
    $('.js .form-item .dependency').each(function() {
      $(this).ancestors('.form-item:last').find('label:first').each(function() {
        if (!$(this).find('.form-required').length) {
          $(this).append(Drupal.settings.required).find('.form-required').hide();
        }
      });
    });
  }
}

/**
 * Function for initiating a form dependency for each dependent element.
 */
Drupal.formDependencies = function (element, settings) {

  // Loop through the form's elements (select, textarea, input) to set the onchange handling.
  $(element.form).find(':input').each(function() {
    var name = $(this).attr('name');
    var node = this.nodeName.toLowerCase();
    var type = (node == 'input' ? $(this).attr('type') : '');

    if (name == settings.dependency || (type == 'checkbox' && name.substring(0, settings.dependency.length) == settings.dependency && name == settings.dependency + '[' + $(this).val() + ']')) {
      // Checkboxes don't invoke the 'onchange' event in IE correctly but 'onclick' works for all
      // browsers on clicking both the label and the checkbox itself.
      if (type == 'checkbox') {
        $(this).click(function() {
          Drupal.formDependenciesCheck(element, settings);
        });
      }
      else {
        $(this).change(function() {
          Drupal.formDependenciesCheck(element, settings);
        });
      }
    }
  });

  Drupal.formDependenciesCheck(element, settings);

};

/**
 * Function for checking the dependency.
 */
Drupal.formDependenciesCheck = function(element, settings) {
  var data = Drupal.formDependenciesValues(element.form, settings.dependency);
  if (data.values.length) {
    //element.disabled = false;
    $(element).ancestors('.form-item').show();
    if (settings.required) $(element).ancestors('.form-item:last').find('label:first .form-required').show();
    if (settings.fieldset) $(element).parent('fieldset').is('.collapsed').removeClass('collapsed');
  }
  else {
    //element.disabled = true;
    $(element).ancestors('.form-item').hide();
    if (settings.fieldset) $(element).parent('fieldset').not('.collapsed').addClass('collapsed');
  }
}

/**
 * Function for retrieving the value of a dependency field's parent. It handles radio inputs,
 * checkbox inputs, textareas, regular inputs, select and multiple selects.
 */
Drupal.formDependenciesValues = function(form, name) {
  var values = [];
  var elements = [];
  $(form).find(':input').each(function() {
    var nname = $(this).attr('name');
    var nnode = this.nodeName.toLowerCase();
    var ntype = (nnode == 'input' ? $(this).attr('type') : '');

    if (nname == name || (ntype == 'checkbox' && nname.substring(0, name.length) == name && nname == name + '[' + $(this).val() + ']')) {
      if (nnode == 'select') {
        elements.push(this);
        if ($(this).attr('multiple')) {
          for (var i = 0; i < this.options.length; i++) {
            if (this.options[i].selected && Drupal.formDependenciesValueCheck(this.options[i].value)) values.push(this.options[i].value);
          }
        }
        else
          values.push(this.options[this.selectedIndex].value);
      }
      else if (nnode == 'textarea' && Drupal.formDependenciesValueCheck(this.innerHTML)) {
        elements.push(this);
        values.push(this.innerHTML);
      }
      else if (nnode == 'input' && (ntype == 'checkbox' || ntype == 'radio') && Drupal.formDependenciesValueCheck($(this).val())) {
        if (this.checked) {
          elements.push(this);
          values.push($(this).val());
        }
      }
      else if (Drupal.formDependenciesValueCheck($(this).val())) {
        elements.push(this);
        values.push($(this).val());
      }
    }
  });
  return {elements: elements, values: values};
};

/**
 * Function for checking a single value for having a length of at least one character.
 */
Drupal.formDependenciesValueCheck = function(value) {
  if (typeof(value.length) != 'undefined') return value.length;
  return false;
}

// Global Killswitch on the <html> element
if (Drupal.jsEnabled) {
  $(document).ready(function() {

    // Initiate page-wide dependency things.
    Drupal.formDependenciesInit();

    // Find and initiate dependency elements.
    $('input.form_dependencies').each(function() {
      var settings = Drupal.parseJson($(this).val());
      var element = $('#'+ settings.field).get(0);
      if (element) {
        Drupal.formDependencies(element, settings);
      }
    });
  });
}
